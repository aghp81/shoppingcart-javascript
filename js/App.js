const products = [
    {
        id: 1,
        name: 'iphone 12',
        price: 99,
        image: './img/apple-iphone-12-black.jpg',
    },

    {
        id: 2,
        name: 'AirPods',
        price: 89,
        image: './img/airpod.jpg',
    }
]

const renderProducts = () => {
    const productDiv = document.querySelector('.products')
    productDiv.innerHTML = ''

}